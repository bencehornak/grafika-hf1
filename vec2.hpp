#ifndef VEC2_HPP
#define VEC2_HPP

/** Simple 2D point */
struct vec2 {
    float x, y;

    vec2(float x = 0, float y = 0) : x(x), y(y) {
    }

    vec2& operator+=(vec2 v) {
        x += v.x;
        y += v.y;
        return *this;
    }
};

vec2 operator*(float lambda, vec2 v) {
    return vec2(lambda * v.x, lambda * v.y);
}

#endif /* VEC2_HPP */

