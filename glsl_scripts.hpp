#ifndef GLSL_SCRIPTS_HPP
#define GLSL_SCRIPTS_HPP

const char* GENERAL_VERTEX_SHADER_PROGRAMS[1] = {R"(
    #version 330
    precision highp float;
    
    uniform mat4 MVP;
    
    // AttribArray 0 -> coords
    layout(location = 0) in vec2 coords;
    // AttribArray 1 -> colors
    layout(location = 1) in vec3 vertexColor;
    
    out vec3 color;
    
    void main() {
        gl_Position = vec4(coords.x, coords.y, 0, 1) * MVP;
        color = vertexColor;
    }
)"};
GLint generalMvpRef;
GLint butterflyTransform2Ref;
GLint butterflyMvpRef;

const char* GENERAL_FRAGMENT_SHADER_PROGRAMS[1] = {R"(
    #version 330
    precision highp float;
    
    in vec3 color;
    out vec4 fragmentColor;
    
    void main() {
        fragmentColor = vec4(color,1);
    }
)"};

const char* BUTTERFLY_VERTEX_SHADER_PROGRAMS[1] = {R"(
    #version 330
    precision highp float;
    
    uniform mat4 MVP;
    uniform mat4 transform2;
    
    // AttribArray 0 -> coords
    layout(location = 0) in vec2 coords;
    
    out vec2 texCoords;
    
    
    void main() {
        vec4 coordsExt = vec4(coords.x, coords.y, 0, 1);
        gl_Position = coordsExt * MVP;
        vec4 texCoordsExt = coordsExt * transform2;
        texCoords = vec2(texCoordsExt.x,
                         texCoordsExt.y);
    }
)"};

const char *BUTTERFLY_FRAGMENT_SHADER_PROGRAMS[1] = {R"(
    #version 330
    precision highp float;
    
    in vec2 texCoords;
    out vec3 fragmentColor;
    
    void main() {
        vec2 f1 = vec2(-.25, -.2), f2 = vec2(.25, -.2);
        float _2a = .27;
    
        float cmp = abs(distance(texCoords, f1) - distance(texCoords, f2));
        if(cmp < _2a)
            fragmentColor = vec3(23.0/255, 12.0/255, 31.0/255);
        else
            fragmentColor = vec3(.30,.20,.40);
    }
)"};

#endif /* GLSL_SCRIPTS_HPP */

