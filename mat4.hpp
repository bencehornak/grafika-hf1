#ifndef MAT4_HPP
#define MAT4_HPP

/** Row-major 4x4 float precision matrix */
struct mat4 {
    GLfloat m[4][4];
public:

    mat4() {
    }

    mat4(GLfloat m00, GLfloat m01, GLfloat m02, GLfloat m03,
            GLfloat m10, GLfloat m11, GLfloat m12, GLfloat m13,
            GLfloat m20, GLfloat m21, GLfloat m22, GLfloat m23,
            GLfloat m30, GLfloat m31, GLfloat m32, GLfloat m33) {
        m[0][0] = m00;
        m[0][1] = m01;
        m[0][2] = m02;
        m[0][3] = m03;
        m[1][0] = m10;
        m[1][1] = m11;
        m[1][2] = m12;
        m[1][3] = m13;
        m[2][0] = m20;
        m[2][1] = m21;
        m[2][2] = m22;
        m[2][3] = m23;
        m[3][0] = m30;
        m[3][1] = m31;
        m[3][2] = m32;
        m[3][3] = m33;
    }

    mat4 operator*(const mat4& right) const {
        mat4 result;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result.m[i][j] = 0;
                for (int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
            }
        }
        return result;
    }

    operator const GLfloat*() const {
        return &m[0][0];
    }

    /** Returns identity matrix */
    static mat4 identity() {
        return mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
                );
    }

    /** Returns the matrix of rotating around z axis */
    static mat4 rotateX(GLfloat phi) {
        float sin = sinf(phi), cos = cosf(phi);
        return mat4(
                1, 0, 0, 0,
                0, cos, sin, 0,
                0, -sin, cos, 0,
                0, 0, 0, 1
                );
    }

    /** Returns the matrix of rotating around z axis */
    static mat4 rotateZ(GLfloat phi) {
        float sin = sinf(phi), cos = cosf(phi);
        return mat4(
                cos, sin, 0, 0,
                -sin, cos, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
                );
    }

    /** Returns a translation matrix */
    static mat4 translate(GLfloat tx, GLfloat ty, GLfloat tz = 0) {
        return mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                tx, ty, tz, 1
                );
    }

    static mat4 translate(const vec4& t) {
        return translate(t.dx(), t.dy(), t.dz());
    }

    static mat4 scale(GLfloat sx, GLfloat sy, GLfloat sz = 0) {
        return mat4(
                sx, 0, 0, 0,
                0, sy, 0, 0,
                0, 0, sz, 0,
                0, 0, 0, 1
                );
    }

    /** x -> rx, y -> ry, z -> rz */
    static mat4 rotate(const vec4& rx, const vec4& ry, const vec4& rz) {
        return mat4(
                rx.dx(), rx.dy(), rx.dz(), 0,
                ry.dx(), ry.dy(), ry.dz(), 0,
                rz.dx(), rz.dy(), rz.dz(), 0,
                0, 0, 0, 1
                );
    }

#if USE_PERSPECTIVE_PROJECTION == 1
    static mat4 project() {
        return mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, -1,
                0, 0, 0, 1
                );
    }
#else
#warning "No perspective projection :'("
    static mat4 project() {
        return mat4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 0, 0,
                0, 0, 0, 1
                );
    }
#endif
};

#endif /* MAT4_HPP */

