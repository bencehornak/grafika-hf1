#ifndef FLOWER_HPP
#define FLOWER_HPP

struct Flower : public Drawable<FLOWER_POINTS> {
private:
    FlowerCurve curve;
    GLfloat x, y, size;

    void genPoints() {
        curve.genCoords(FLOWER_POINTS, coords);

        colors[0] = 0.75;
        colors[1] = 0.03;
        colors[2] = 0;

        // #F5BD42
        for (int i = 1; i < FLOWER_POINTS; i++) {
            colors[3 * i + 0] = .96;
            colors[3 * i + 1] = .74;
            colors[3 * i + 2] = .26;
        }

    }
public:

    Flower(int numPetals, GLfloat x, GLfloat y, GLfloat size)
    : curve(numPetals), x(x), y(y), size(size) {
        genPoints();
    }

    mat4 mvp() override {
        return mat4::scale(size, size) * mat4::translate(x, y);
    }

};

#endif /* FLOWER_HPP */

