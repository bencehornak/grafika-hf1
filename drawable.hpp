#ifndef DRAWABLE_HPP
#define DRAWABLE_HPP

struct World;

template<int numPoints>
struct Drawable {

protected:
    GLuint vao;

    GLfloat t;
    GLfloat coords[2 * numPoints];
    GLfloat colors[3 * numPoints];

public:

    virtual ~Drawable() {
    }

    virtual void init() {
        GLuint vbo[2];
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glGenBuffers(2, vbo);

        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof (coords), coords, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);


        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof (colors), colors, GL_STATIC_DRAW);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    }

    virtual void update(GLfloat t) {
        this->t = t;
    }

    virtual mat4 mvp() = 0;

    virtual void useProgram();

    virtual void draw() {
        useProgram();

        glBindVertexArray(vao);
        glUniformMatrix4fv(generalMvpRef, 1, GL_TRUE, mvp());

        glDrawArrays(GL_TRIANGLE_FAN, 0, numPoints);
    }

};

#endif /* DRAWABLE_HPP */

