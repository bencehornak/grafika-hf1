#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

int fibonacci(int n) {
    if (n <= 0)
        return 0;
    int a = 0, b = 1;
    for (int i = 1; i < n; ++i) {
        int c = a + b;
        a = b;
        b = c;
    }
    return b;
}

#endif /* FIBONACCI_HPP */

