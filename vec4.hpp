#ifndef VEC4_HPP
#define VEC4_HPP

/** 3D point in homogeneous coordinates */
struct vec4 {
private:
    /** Homogeneous coordinates */
    GLfloat _hx, _hy, _hz, _hw;

public:

    vec4(GLfloat hx = 0, GLfloat hy = 0, GLfloat hz = 0, GLfloat hw = 1)
    : _hx(hx), _hy(hy), _hz(hz), _hw(hw) {
    }

    vec4(const vec2& v2) : _hx(v2.x), _hy(v2.y), _hz(0), _hw(1) {
    }

    /** Homogeneous x coordinate */
    GLfloat hx() const {
        return _hx;
    }

    /** Homogeneous y coordinate */
    GLfloat hy() const {
        return _hy;
    }

    /** Homogeneous z coordinate */
    GLfloat hz() const {
        return _hz;
    }

    /** Homogeneous w coordinate */
    GLfloat hw() const {
        return _hw;
    }

    /** Descartes x coordinate */
    GLfloat dx() const {
        return _hx / _hw;
    }

    /** Descartes y coordinate */
    GLfloat dy() const {
        return _hy / _hw;
    }

    /** Descartes z coordinate */
    GLfloat dz() const {
        return _hz / _hw;
    }

    GLfloat norm() const {
        return sqrt(_hx * _hx + _hy * _hy + _hz * _hz) / _hw;
    }

    vec4 normalize() const;

    vec4 cross(const vec4& v) const {
        GLfloat x1 = dx(), y1 = dy(), z1 = dz();
        GLfloat x2 = v.dx(), y2 = v.dy(), z2 = v.dz();

        return vec4(
                +y1 * z2 - z1 * y2,
                -x1 * z2 + z1 * x2,
                +x1 * y2 - x2 * y1
                );
    }

    GLfloat dot(const vec4& v) const {
        return (_hx * v._hx + _hy * v._hy + _hz * v._hz) / (_hw * v._hw);
    }

    vec4 operator-(const vec4& v) const {
        return vec4(dx() - v.dx(), dy() - v.dy(), dz() - v.dz());
    }

    vec4 operator+(const vec4& v) const {
        return vec4(dx() + v.dx(), dy() + v.dy(), dz() + v.dz());
    }

    vec4& operator+=(const vec4& v) {
        (*this) = (*this) + v;
        return *this;
    }

    void print(const char* name = "") const {
        printf("%s: (%f,%f,%f)\n", name, dx(), dy(), dz());
    }
};

vec4 operator*(GLfloat lambda, const vec4& v) {
    return vec4(lambda * v.hx(), lambda * v.hy(), lambda * v.hz(), v.hw());
}

vec4 vec4::normalize() const {
    return 1 / norm() * (*this);
}

#endif /* VEC4_HPP */

