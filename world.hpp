#ifndef WORLD_HPP
#define WORLD_HPP


struct World {
private:
    GLuint generalShaderProgram, butterflyShaderProgram;
    bool created;

    // Model objects
    Butterfly *butterfly;
    Flower *flowers[NUM_FLOWERS];

    bool mouseDown;
    vec2 mousePos;

    GLfloat prev_t;

    GLuint createVertexShader(int numPrograms, const char** programs) {
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        if (!vertexShader) {
            printf("Could not create vertex shader\n");
            exit(1);
        }
        glShaderSource(vertexShader, numPrograms, programs,
                nullptr);
        glCompileShader(vertexShader);
        checkShader(vertexShader, "Error in vertex shader program");
        return vertexShader;
    }

    GLuint createFragmentShader(int numPrograms, const char** programs) {
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        if (!fragmentShader) {
            printf("Could not create fragment shader\n");
            exit(1);
        }
        glShaderSource(fragmentShader, numPrograms, programs,
                nullptr);
        glCompileShader(fragmentShader);
        checkShader(fragmentShader, "Error in fragment shader program");
        return fragmentShader;
    }

    void createGeneralShaderProgram() {
        GLuint vertexShader = createVertexShader(1, GENERAL_VERTEX_SHADER_PROGRAMS);
        GLuint fragmentShader = createFragmentShader(1, GENERAL_FRAGMENT_SHADER_PROGRAMS);

        generalShaderProgram = glCreateProgram();
        if (!generalShaderProgram) {
            printf("Could not create general shader program\n");
            exit(1);
        }

        glAttachShader(generalShaderProgram, vertexShader);
        glAttachShader(generalShaderProgram, fragmentShader);

        // fragmentColor -> color output
        glBindFragDataLocation(generalShaderProgram, 0, "fragmentColor");

        glLinkProgram(generalShaderProgram);
        checkLinking(generalShaderProgram);

        // Get mvp reference
        generalMvpRef = glGetUniformLocation(generalShaderProgram, "MVP");
        if (generalMvpRef < 0) {
            printf("Could not get general mvp reference\n");
            exit(1);
        }
    }

    void createButterflyShaderProgram() {
        GLuint vertexShader = createVertexShader(1, BUTTERFLY_VERTEX_SHADER_PROGRAMS);
        GLuint fragmentShader = createFragmentShader(1, BUTTERFLY_FRAGMENT_SHADER_PROGRAMS);

        butterflyShaderProgram = glCreateProgram();
        if (!butterflyShaderProgram) {
            printf("Could not create butterfly shader program\n");
            exit(1);
        }

        glAttachShader(butterflyShaderProgram, vertexShader);
        glAttachShader(butterflyShaderProgram, fragmentShader);

        // fragmentColor -> color output
        glBindFragDataLocation(butterflyShaderProgram, 0, "fragmentColor");

        glLinkProgram(butterflyShaderProgram);
        checkLinking(butterflyShaderProgram);

        glUseProgram(butterflyShaderProgram);
        // Get mvp reference
        butterflyMvpRef = glGetUniformLocation(butterflyShaderProgram, "MVP");
        if (butterflyMvpRef < 0) {
            printf("Could not get butterfly mvp reference\n");
            exit(1);
        }
        // Get transform2 reference
        butterflyTransform2Ref = glGetUniformLocation(butterflyShaderProgram, "transform2");
        if (butterflyTransform2Ref < 0) {
            printf("Could not get butterfly transform2 reference\n");
            exit(1);
        }
    }

    void createModelObjects() {
        butterfly = new Butterfly;

        srand(RANDOM_SEED);

        for (int i = 0; i < NUM_FLOWERS; i++) {
            int fib = rand() % (FLOWER_MAX_FIBONACCI - FLOWER_MIN_FIBONACCI + 1)
                    + FLOWER_MIN_FIBONACCI;
            GLfloat x = (GLfloat) rand() / RAND_MAX * 2 - 1;
            GLfloat y = (GLfloat) rand() / RAND_MAX * 2 - 1;
            flowers[i] = new Flower(fib, x, y, FLOWER_SIZE);

        }

    }

    void destroyModelObjects() {
        delete butterfly;
        for (int i = 0; i < NUM_FLOWERS; i++) {
            delete flowers[i];
        }

    }

    void initModelObjects() {
        butterfly->init();
        for (int i = 0; i < NUM_FLOWERS; i++) {
            flowers[i]->init();
        }

    }

    void updateModelObjects(GLfloat t) {
        butterfly->update(t, t - prev_t, mouseDown, mousePos);
        prev_t = t;
    }

    void drawModelObjects() {
        for (int i = 0; i < NUM_FLOWERS; i++) {
            flowers[i]->draw();
        }
        butterfly->draw();

    }
public:

    World() : created(false), mouseDown(false), mousePos() {
        createModelObjects();
    }

    void init() {
        created = true;

        createGeneralShaderProgram();
        createButterflyShaderProgram();
        initModelObjects();
    }

    void update(GLfloat t) {
        updateModelObjects(t);
    }

    void draw() {
        // #386E37
        glClearColor(.22, .43, .22, 1); // background color 
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        drawModelObjects();

        glutSwapBuffers();
    }

    ~World() {
        destroyModelObjects();
        if (created) {
            glDeleteProgram(generalShaderProgram);
            glDeleteProgram(butterflyShaderProgram);
        }
    }

    void setMouseState(bool mouseDown, vec2 mousePos = vec2()) {
        this->mouseDown = mouseDown;
        this->mousePos = mousePos;
    }
    
    void useGeneralShader() {
        glUseProgram(generalShaderProgram);
    }
    void useButterflyShader() {
        glUseProgram(butterflyShaderProgram);
    }
};

#endif /* WORLD_HPP */

