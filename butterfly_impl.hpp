#ifndef BUTTERFLY_IMPL_HPP
#define BUTTERFLY_IMPL_HPP



vec4 Butterfly::gravity(0, 0, GRAVITY);

Butterfly::Butterfly() {
    create();
}

Butterfly::~Butterfly() {
    destroy();
}

void Butterfly::create() {
    leftWing = new Wing(*this, true);
    rightWing = new Wing(*this, false);
    body = new Body(*this);

    a = vec4(.0001);
    v = vec4(0, .0001);
    r = vec4(0, 0, 0);
    i = vec4(1, 1).normalize();
    j = vec4(-1, 1).normalize();
    k = vec4(0, 0, 1).normalize();
}

void Butterfly::destroy() {
    delete leftWing;
    delete rightWing;
    delete body;
}

void Butterfly::init() {
    leftWing->init();
    rightWing->init();
    body->init();
}

void Butterfly::update(GLfloat t, GLfloat dt, bool mouseDown, const vec2& mousePos) {
    updatePhysics(dt, mouseDown, mousePos);
    leftWing->update(t);
    rightWing->update(t);
    body->update(t);
}

void Butterfly::draw() {
    leftWing->draw();
    rightWing->draw();
    body->draw();
}

mat4 Butterfly::mvp() const {
    mat4 rotate = mat4::rotate(i, j, k);
    
    mat4 scale = mat4::scale(
            BUTTERFLY_SIZE,
            BUTTERFLY_SIZE,
            BUTTERFLY_SIZE
            );
    mat4 translate = mat4::translate(r);
    mat4 proj = mat4::project();
    return scale * rotate * translate * proj;
}

void Butterfly::updatePhysics(GLfloat dt, bool mouseDown, vec2 mousePos) {
    a = gravity - DRAG * v;
    if (mouseDown) {
        a += SPRING_CONSTANT * ((vec4) mousePos - r);
    }
    v += dt * a;
    v = vec4(v.dx(), v.dy()); // Project to xy plane
    r += dt * v;

    j = v.normalize();
    i = j.cross(a).normalize();
    k = i.cross(j);
}

#endif /* BUTTERFLY_IMPL_HPP */

