#ifndef BUTTERFLY_DRAWABLE_HPP
#define BUTTERFLY_DRAWABLE_HPP

template<int numPoints>
struct ButterflyDrawable : public Drawable<numPoints> {
private:
    const Butterfly& butterfly;
public:

    ButterflyDrawable(const Butterfly& butterfly) : butterfly(butterfly) {
    }

    void useProgram() override;

    void init() override {
        GLuint vbo;
        glGenVertexArrays(1, &this->vao);
        glBindVertexArray(this->vao);
        glGenBuffers(1, &vbo);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof (this->coords), this->coords, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    }

    virtual mat4 transform2() = 0;

    mat4 mvp() override {
        return butterfly.mvp();
    }

    void draw() override {
        useProgram();

        glBindVertexArray(this->vao);
        glUniformMatrix4fv(butterflyMvpRef, 1, GL_TRUE, mvp());
        glUniformMatrix4fv(butterflyTransform2Ref, 1, GL_TRUE, transform2());


        glDrawArrays(GL_TRIANGLE_FAN, 0, numPoints);
    }



};

#endif /* BUTTERFLY_DRAWABLE_HPP */

