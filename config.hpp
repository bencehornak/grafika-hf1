#ifndef CONFIG_HPP
#define CONFIG_HPP

#define RANDOM_SEED ( 3742823 )

#define USE_PERSPECTIVE_PROJECTION ( 1 )

#define WING_CURVE1_POINTS ( 50 )
#define WING_CURVE2_POINTS ( 50 )
#define WING_CURVE_POINTS (WING_CURVE1_POINTS + WING_CURVE2_POINTS)

#define BODY_POINTS ( 50 )

#define NUM_FLOWERS ( 5 )
#define FLOWER_POINTS ( 100 )
#define FLOWER_SIZE ( .2 )
#define FLOWER_MIN_FIBONACCI ( 5 )
#define FLOWER_MAX_FIBONACCI ( 7 )

#define BUTTERFLY_SIZE ( .3 )

#define SPRING_CONSTANT ( .5 )
#define GRAVITY ( .1 )

#define DRAG ( .01 )

#endif /* CONFIG_HPP */

